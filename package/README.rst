=============================
mayan-package
=============================

.. image:: https://badge.fury.io/py/package.png
    :target: https://badge.fury.io/py/package

Your project description goes here

Documentation
-------------

The full documentation is at https://package.readthedocs.org.

Quickstart
----------

Install mayan-package::

    pip install package

Then use it in a project::

    import package

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements-test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-pypackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
