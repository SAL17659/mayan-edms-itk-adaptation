from __future__ import unicode_literals

from kombu import Exchange, Queue

from django.apps import apps
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from common import menu_object, menu_secondary, menu_setup
from common.apps import MayanAppConfig
from documents.models import Document
from documents.search import document_search
from documents.signals import post_document_created
from mayan.celery import app
from navigation import SourceColumn
from rest_api.classes import APIEndPoint

from .handlers import initialize_new_document_data
from .links import link_document_extra_data_view, link_extra_data_list


class DocumentExtraDataApp(MayanAppConfig):
    # MayanAppConfig is a subclass of Django's AppConfig and provides
    # additional services.
    
    # App name
    name = 'package'
    
    # Set to True if this app provides tests
    test = True
    
    # verbose_name will be shown in the admin interface as the apps' name
    verbose_name = _('Your project description goes here')

    def ready(self):
        # Always include super
        super(DocumentExtraDataApp, self).ready()
        
        # Use apps.get_model for foreign models
        # Use self.get_model for this app models
        Document = apps.get_model(
            app_label='documents', model_name='Document'
        )

        # Use APIEndPoint to publish REST API URLs 
        APIEndPoint(app=self, version_string='1')

        # SourceColumn allows registering new list columns to an existing
        # model. SourceColumns can be attributes or functions.
        SourceColumn(
            source=Document, label=_('Extra data'),
            attribute='extra_data.data'
        )
        
        # Add a column sourced from a function.
        SourceColumn(
            source=Document, label=_('Data size'),
            func=lambda context: len(context['object'].extra_data.data),
        )

        # If your app uses background task you must configure on which queue
        # it will be placed. You can use existing queues from other apps or
        # create your own. Refer to your task using a full path
        # "<app_name>.tasks.<task function name>"
        app.conf.CELERY_ROUTES.update(
            {
                'package.tasks.task_extra_data': {
                    'queue': 'my_queue'
                },
            }
        )
        
        # If you create a new queue, you need to create and exchange and
        # routing key for it. For most typical cases the same queue name
        # can be used.
        app.conf.CELERY_QUEUES.append(
            Queue('my_queue', Exchange('my_queue'), routing_key='my_queue'),
        )

        # To make new fields searchable add them to the document_search
        # instance from the documents.search module.
        document_search.add_model_field(
            field='extra_data__data', label=_('Extra data')
        )
        
        # Bind your links to a menu and an class or view. This will display
        # an HTML anchor to a resolved view in the "actions" column of each
        # table row for each instance of the Document class. Every time a 
        # list of documents is displayed a your link will be added to the 
        # possible actions for each entry in the list.
        menu_object.bind_links(
            links=(link_document_extra_data_view,), sources=(Document,)
        )

        # As an a secondary action to displayed in the document detail view.
        # To bind a link to a view regardless of the presence of a class 
        # instance, pass a the view name as a string in the sources
        # parameter.
        
        # menu_secondary.bind_links(
        #     links=(link_extra_data,),
        #     sources=(
        #         Document, 'package:sequence_create',
        #     )
        # )

        # As a link in the Setup menu.
        menu_setup.bind_links(links=(link_sequence_list,))

        # You can also disable links registered (binded) by other apps by 
        # calling the .unbind method of a menu instance.

        # Connect your signal handlers to Django or Mayan EDMS signals.
        post_document_created.connect(
            initialize_new_document_data,
            dispatch_uid='initialize_new_document_data', sender=Document
        )
