from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from navigation import Link

# Links test for permission only for display purposes onyl, a permission
# must also be specified in the views.
# The only expection is multi item (list) views, they test each entry for 
# access.
from .permissions import permission_extra_data_view

# Follow variable naming conventions
link_extra_data_list = Link(
    permissions=(permission_extra_data_view,),
    text=_('List extra data'),
    view='package:extra_data_list'
)
link_document_extra_data_view = Link(
    permissions=(permission_extra_data_view,), text=_('View extra data'),
    view='package:document_extra_data_view', args='resolved_object.id'
)
