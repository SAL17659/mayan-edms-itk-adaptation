from __future__ import unicode_literals

# Split tests by what they test, first by module then by TestCase subclass.

from django.contrib.auth import get_user_model
from django.core.files.base import File
from django.test import TestCase, override_settings

from documents.models import DocumentType, Document
from documents.tests import TEST_DOCUMENT_TYPE
from user_management.tests.literals import (
    TEST_USER_PASSWORD, TEST_USER_USERNAME
)

from ..literals import DEFAULT_EXTRA_DATA
from ..models import DocumentExtraData

from .literals import TEST_SMALL_DOCUMENT_PATH


# To speed up tests, turn off global OCR processing.
@override_settings(OCR_AUTO_OCR=False)
class ExtraDataTestCase(TestCase):
    def setUp(self):
        self.document_type = DocumentType.objects.create(
            label=TEST_DOCUMENT_TYPE
        )

    def tearDown(self):
        self.document_type.delete()

    def test_default_document_extra_data(self):
        user = get_user_model().objects.create_user(
            username=TEST_USER_USERNAME
        )
        
        with open(TEST_SMALL_DOCUMENT_PATH) as file_object:
            document = self.document_type.new_document(
                file_object=File(file_object), _user=user
            )

        document = Document.objects.first()

        self.assertEqual(document.label, TEST_USER_USERNAME)

    def test_document_event_log_template(self):
        self.document_type.renaming_template.template = TEST_TEMPLATE_USER_CREATOR
        self.document_type.renaming_template.save()

        with open(TEST_SMALL_DOCUMENT_PATH) as file_object:
            document = self.document_type.new_document(
                file_object=File(file_object), _user=user
            )

        document = Document.objects.first()

        self.assertEqual(document.extra_data.data, DEFAULT_EXTRA_DATA)
