from __future__ import unicode_literals

import logging

from django.apps import apps

from .tasks import task_extra_data

logger = logging.getLogger(__name__)


def initialize_new_document_data(sender, instance, **kwargs):
    """
    Create a blank extra data when a new document is created
    """
    # Make liberal use of logging
    logger.debug('instance: %s', instance)

    # Do not import models directly, use the get_model helper function
    DocumentExtraData = apps.get_model(
        app_label='package',
        model_name='DocumentExtraData'
    )

    if kwargs['created']:
        DocumentExtraData.objects.create(document=instance)
        
        # Pass only simple data types to background tasks. If a model
        # instance needs to be referenced, pass the instance's primary key,
        # the task must get a fresh copy of the instance when it finally
        # executes.
        task_extra_data.apply_async(kwargs={'document_pk': instance.pk})



