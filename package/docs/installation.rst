============
Installation
============

At the command line::

    $ easy_install package

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv package
    $ pip install package
