from mayan.apps import cabinets
from .models import Profile
from .links import link_profile_edit
from mayan.apps.views.generics import (
    MultipleObjectFormActionView, SingleObjectCreateView,
    SingleObjectDeleteView, SingleObjectEditView, SingleObjectListView
)

from django.http import HttpResponse
import datetime
from mayan.apps.cabinets.models import Cabinet

from mayan.apps.views.generics import SingleObjectEditView
from django.urls import reverse_lazy

##This whole app has Flask as a requirement to properly work
"""
def EditAktnumberConfView(request):
    cabinet = Cabinet.objects.all().first()
    user = request.user
    profile = Profile.objects.filter(user=user)[0]
    
    html = open("mayan/apps/profile/templates/index.html").read().format(v1=user.username, v2=profile.aktnumber_conf)
    
    return HttpResponse(html)

def validate_aktnumber_conf(request):
    aktnumber_conf = request.GET.get('aktnumber_conf', None)
"""    

class EditAktnumberView(SingleObjectEditView):
    fields = ('aktnumber_conf',)
    model = Profile
    post_action_redirect = reverse_lazy(viewname='cabinets:cabinet_list')
    pk_url_kwarg = 'id'

    def get_extra_context(self):
        return {
            'object': self.object,
            'title': ('Edit cabinet: %s') % self.object,
        }

    def get_instance_extra_data(self):
        return {'_event_actor': self.request.user}



"""
class EditAktnumberConfView(SingleObjectCreateView):
    fields = ('aktnumber_conf',)
    model = Profile

    def get_extra_context(self):
        return {
            'object': self.object,
            'title': ('Edit Aktnumber - Bitte *noch* nicht verwenden'),
        }

    def get_instance_extra_data(self):
        return {'_event_actor': self.request.user}

"""