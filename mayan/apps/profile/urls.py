from django.conf.urls import url
from mayan.apps.profile.views import EditAktnumberView

app_name='profile'

urlpatterns = [
    url(
        regex=r'^profile/(?P<id>\d+)/edit/$', name='profile_edit',
        view=EditAktnumberView.as_view()
    )
]
