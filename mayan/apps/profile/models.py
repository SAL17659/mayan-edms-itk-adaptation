from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    aktnumber_conf = models.CharField(max_length=50, blank=True, default="TEST", help_text=('Hier wird der Syntax zu finden sein, nach welchem sich die Aktnumber richten soll (z.B.: [[year]][[month]][[name]])'))

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()