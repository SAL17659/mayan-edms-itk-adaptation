from mayan.apps.navigation.classes import Link
from django.utils.translation import ugettext_lazy as _
from mayan.apps.cabinets.icons import icon_cabinet_edit

app_name='profile'

link_profile_edit = Link(
    args='object.pk',
    text=_('Aktnummer-Konfiguration'),
    view='profile:profile_edit',
    icon=icon_cabinet_edit
)