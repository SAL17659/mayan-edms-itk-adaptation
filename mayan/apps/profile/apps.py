from django.utils.translation import ugettext_lazy as _
from mayan.apps.common.apps import MayanAppConfig


from django.apps import apps
from mayan.apps.common.menus import menu_main
from .links import link_profile_edit

from mayan.apps.events.classes import EventModelRegistry

class ProfileApp(MayanAppConfig):
    app_namespace = 'profile'
    app_url = 'profile'
    has_rest_api = False
    has_tests = True
    name = 'mayan.apps.profile'
    verbose_name = _('Profile')

    def ready(self):
        super(ProfileApp, self).ready()

        Profile = apps.get_model(
            app_label='profile', model_name='Profile'
        )

        menu_main.bind_links(links=(link_profile_edit,), position=98)

        

        