# Generated by Django 2.2.23 on 2021-06-30 06:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cabinets', '0007_auto_20210628_0730'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cabinet',
            name='isAkt',
            field=models.BooleanField(default=False, help_text='Check this box if you want to create an act.', verbose_name='Akt'),
        ),
    ]
