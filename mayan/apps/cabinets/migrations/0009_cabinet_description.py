# Generated by Django 2.2.23 on 2021-07-19 09:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cabinets', '0008_auto_20210630_0641'),
    ]

    operations = [
        migrations.AddField(
            model_name='cabinet',
            name='description',
            field=models.TextField(blank=True, help_text='A text to describe what the cabinet is about.', max_length=256, null=True, verbose_name='Description'),
        ),
    ]
