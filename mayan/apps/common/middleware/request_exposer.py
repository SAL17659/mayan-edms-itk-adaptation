from django.db import models
from django.conf import settings

def RequestExposerMiddleware(get_response):
    def middleware(request):
        models.exposed_request = request
        response = get_response(request)
        return response
    return middleware