# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Sergey Glita <gsv70@mail.ru>, 2021
# lilo.panic, 2021
# Panasoft, 2021
# OLeg Si <olegsm35@gmail.com>, 2021
# Ilya Pavlov <spirkaa@gmail.com>, 2021
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-05-19 03:26+0000\n"
"PO-Revision-Date: 2021-04-12 03:39+0000\n"
"Last-Translator: Ilya Pavlov <spirkaa@gmail.com>, 2021\n"
"Language-Team: Russian (https://www.transifex.com/rosarior/teams/13584/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: apps.py:34 events.py:5 permissions.py:5
msgid "Messaging"
msgstr "Сообщения"

#: apps.py:56
msgid "None"
msgstr "Нет"

#: apps.py:62
msgid "Sender"
msgstr "Отправитель"

#: events.py:8
msgid "Message created"
msgstr "Сообщение создано"

#: events.py:11
msgid "Message edited"
msgstr "Сообщение изменено"

#: forms.py:9 models.py:38
msgid "Body"
msgstr "Содержимое"

#: links.py:28 views.py:33
msgid "Create message"
msgstr "Создать объявление"

#: links.py:31 links.py:37
msgid "Delete"
msgstr "Удалить"

#: links.py:45 links.py:49
msgid "Mark as read"
msgstr "Отметить как прочитанное"

#: links.py:53
msgid "Mark all as read"
msgstr "Отметить все как прочитанные"

#: models.py:30
msgid "User"
msgstr "Пользователь"

#: models.py:33
msgid "Short description of this message."
msgstr "Краткое описание сообщения."

#: models.py:34
msgid "Subject"
msgstr "Тема"

#: models.py:37
msgid "The actual content of the message."
msgstr "Фактическое содержимое сообщения."

#: models.py:42
msgid "This field determines if the message has been read or not."
msgstr "Это поле определяет, прочитано сообщение или нет."

#: models.py:43
msgid "Read"
msgstr "Прочитано"

#: models.py:47
msgid "Date and time of the message creation."
msgstr "Дата и время создания сообщения."

#: models.py:48
msgid "Creation date and time"
msgstr "Дата и время создания"

#: models.py:53
msgid "Message"
msgstr "Сообщение"

#: models.py:54 views.py:121
msgid "Messages"
msgstr "Сообщения"

#: models.py:70
msgid "Label"
msgstr "Заголовок"

#: models.py:91
#, python-format
msgid "Template error; %(exception)s"
msgstr "Ошибка в шаблоне; %(exception)s"

#: permissions.py:8
msgid "Create messages"
msgstr "Создать сообщения"

#: permissions.py:11
msgid "Delete messages"
msgstr "Удалить сообщения"

#: permissions.py:14
msgid "View messages"
msgstr "Просмотр сообщений"

#: views.py:34
msgid "Send"
msgstr "Отправить"

#: views.py:46
#, python-format
msgid "Error deleting message \"%(instance)s\"; %(exception)s"
msgstr "Ошибка при удалении сообщения \"%(instance)s\"; %(exception)s"

#: views.py:50
#, python-format
msgid "Message \"%(object)s\" deleted successfully."
msgstr "Сообщение \"%(object)s\" удалено."

#: views.py:51
#, python-format
msgid "%(count)d message deleted successfully."
msgstr "%(count)d сообщение удалено."

#: views.py:52
#, python-format
msgid "%(count)d messages deleted successfully."
msgstr "%(count)d сообщений удалено."

#: views.py:53
#, python-format
msgid "Delete message: %(object)s."
msgstr "Удалить сообщение: %(object)s."

#: views.py:54
#, python-format
msgid "Delete the %(count)d selected message."
msgstr "Удалить %(count)d выбранное сообщение."

#: views.py:55
#, python-format
msgid "Delete the %(count)d selected messages."
msgstr "Удалить %(count)d выбранных сообщений."

#: views.py:88
#, python-format
msgid "Details of message: %s"
msgstr "Подробности сообщения: %s"

#: views.py:116
msgid "Here you will find text messages from other users or from the system."
msgstr ""
"Здесь вы найдете текстовые сообщения от других пользователей или системы."

#: views.py:119
msgid "There are no messages"
msgstr "Нет сообщений"

#: views.py:130
#, python-format
msgid "Error marking message \"%(instance)s\" as read; %(exception)s"
msgstr ""
"Ошибка при отметке сообщения \"%(instance)s\" как прочитанного; "
"%(exception)s"

#: views.py:136
#, python-format
msgid "Message \"%(object)s\" marked as read successfully."
msgstr "Сообщение \"%(object)s\" отмечено как прочитанное."

#: views.py:139
#, python-format
msgid "%(count)d message marked as read successfully."
msgstr "%(count)d сообщение отмечено как прочитанное."

#: views.py:142
#, python-format
msgid "%(count)d messages marked as read successfully."
msgstr "%(count)d сообщений отмечено как прочитанные."

#: views.py:144
#, python-format
msgid "Mark the message \"%(object)s\" as read."
msgstr "Отметить сообщение \"%(object)s\" как прочитанное."

#: views.py:145
#, python-format
msgid "Mark the %(count)d selected message as read."
msgstr "Отметить %(count)d выбранное сообщение как прочитанное."

#: views.py:146
#, python-format
msgid "Mark the %(count)d selected messages as read."
msgstr "Отметить %(count)d выбранных сообщений как прочитанные."

#: views.py:172
msgid "Mark all message as read?"
msgstr "Отметить все сообщения как прочитанные?"

#: views.py:188
msgid "All messages marked as read."
msgstr "Все сообщения отмечены как прочитанные."
